/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "searchengine.h"

std::vector<Result> SearchEngine::search(const std::vector<std::string> input) {
  std::vector<Result> final_results;

  auto partial_results = index->search(input);

  std::vector<std::string> urls;
  for (const auto& element : partial_results)
    urls.push_back(element.url);

  //std::vector <double> rank = page_rank->get_rank(urls);

  for (unsigned i = 0; i < partial_results.size(); i++) {
    final_results.push_back({
      partial_results[i].url,
      partial_results[i].weigth,
      0
      //rank[i]
    });
  }

  // Ordenar vector por importancia relativa del page rank
  std::sort(final_results.begin(), final_results.end(),
            [](const Result& a,
               const Result& b) {
              return a.token_weigth > b.token_weigth;
           });
  /*
  // Ordenar vector por importancia relativa de los tokens
  std::sort(final_results.begin(), final_results.end(),
            [](const Result& a,
               const Result& b) {
              return a.token_weigth > b.token_weigth;
               });

  */
  return final_results;

}
