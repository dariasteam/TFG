/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef URLNORMALIZER_H
#define URLNORMALIZER_H

#include <string>
#include <algorithm>

class URLNormalizer {
private:
  void to_lower (std::string& str);
  void delete_dynamic_parameters (std::string& str);
  void delete_anchors (std::string& str);
  void add_last_path_bar (std::string& str);
public:
  URLNormalizer();
  void normalize (std::string& str);
};

#endif // URLNORMALIZER_H
