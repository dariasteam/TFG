#include "urlnormalizer.h"

URLNormalizer::URLNormalizer() {}

void URLNormalizer::normalize(std::string& str) {
  to_lower(str);
  delete_dynamic_parameters(str);
  delete_anchors (str);
  add_last_path_bar(str);
}

void URLNormalizer::to_lower(std::string &str) {
  std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}

void URLNormalizer::delete_dynamic_parameters(std::string &str) {
  const auto pos = str.find('?');
  if (pos != std::string::npos)
    str.resize(pos);
}

void URLNormalizer::delete_anchors(std::string &str) {
  const auto pos = str.find('#');
  if (pos != std::string::npos)
    str.resize(pos);
}

void URLNormalizer::add_last_path_bar(std::string& str) {
  if (str[str.size() - 1] != '/')
    str.push_back('/');
}

