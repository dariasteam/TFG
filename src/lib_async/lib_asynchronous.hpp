/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIB_ASYNC_H
#define LIB_ASYNC_H

#include <functional>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <queue>
#include <list>
#include <iostream>

#define VERBOSE

class asyncManager {
private:
  std::atomic<bool> end_loop;
  long long n_threads;
  std::mutex m;
  std::condition_variable cv;

  asyncManager() :
  n_threads(0)
  {
    end_loop.store(false);
  }

  ~asyncManager() {
    end_safe();
  }

public:

  static asyncManager& get_instance () {
    static asyncManager instance;
    return instance;
  }

  void start() {
    std::unique_lock<std::mutex> lk(m);
    cv.wait(lk, [&]{return !is_alive();});
  }

  void add_function (std::function<void (void)> function) {
    auto ff = [&, function]() {
      function();
      --n_threads;
    };
    ++n_threads;
    std::thread t (ff);

    t.detach();
  }

  // Returns a function to be called to wake up the thread
  std::function<void (bool)> add_persistent_sleepy_function (
                             std::function<bool (void)> function) {

    std::mutex* l_m = new std::mutex();
    std::condition_variable* l_cv = new std::condition_variable();
    std::unique_lock<std::mutex>* l_lk =
                                       new std::unique_lock<std::mutex> (*l_m);

    bool* wakeup_condition = new bool (false);
    bool* alive_condition = new bool (true);

    auto persistent = [&, function, l_m, l_cv, l_lk, alive_condition,
                                                     wakeup_condition] (void) {
      while (is_alive() && *alive_condition) {
        if (!function ()) {
          *wakeup_condition = false;
          l_cv->wait(*l_lk, [&]{return (*wakeup_condition);});
        }
      }
      --n_threads;

      delete l_m;
      delete l_cv;
      delete l_lk;
      delete wakeup_condition;
      delete alive_condition;
    };

    auto signal = [l_cv, wakeup_condition, alive_condition](bool keep_alive) {
      *wakeup_condition = true;
      *alive_condition = keep_alive;
      l_cv->notify_one();
    };

    ++n_threads;
    std::thread t (persistent);
    t.detach();

    return signal;
  }

  void add_persistent_function (std::function<bool (void)> function) {
    auto persistent = [&, function](void) {
      while (is_alive() && function ());
      --n_threads;
    };
    ++n_threads;
    std::thread t (persistent);
    t.detach();
  }

  bool end_safe () {
    if (!end_loop.load()) {
      end_loop.store(true);
      #ifdef VERBOSE
      std::cerr << "Waiting for threads to finish " << n_threads << std::endl;
      #endif
      while (n_threads > 1) {
        std::this_thread::sleep_for(std::chrono::nanoseconds(3));
      }
      #ifdef VERBOSE
      std::cerr << "All threads finished" << std::endl;
      #endif
      cv.notify_one();
      return true;
    }
    return true;
  }

  void end_unsafe () {
    if (!end_loop.load()) {
      end_loop.store(true);
      cv.notify_one();
    }
  }

  bool is_alive () const { return !end_loop.load(); }
};

class asyncObject {
public:
  ~asyncObject() {};
  void send_signal (std::function<void (void)> slot) {
    asyncManager::get_instance().add_function(slot);
  }
};

template <class T>
class asyncDispatcher : public asyncObject {
private:
  std::mutex _dispatcher_mtx;
  std::queue <T> queue_t;
  std::list <std::function <void (bool)>> wake_ups;
  bool dispatcher_enabled;

  unsigned buffer_size;

  int working_threads;

  void check_finish () {
    _dispatcher_mtx.lock();
    working_threads--;
    //if (working_threads == 0)  {   // FIXME: Why?
      //wake_up_threads();
    //}
    _dispatcher_mtx.unlock();
  }

  bool try_dispatch () {
    _dispatcher_mtx.lock();
    working_threads++;
    _dispatcher_mtx.unlock();

    while (dispatcher_enabled) {
                                      // FIXME: A
      if (queue_t.empty()) {
        //_dispatcher_mtx.unlock();   // FIXME: Enable if moved to A
        check_finish ();
        on_queue_empty();
        return false;
      } else {
        _dispatcher_mtx.lock();       // FIXME move to A
        T element = queue_t.front();
        queue_t.pop();
        _dispatcher_mtx.unlock();
        operation (element);
      }
    }

    check_finish ();
    return false;
  }

  void wake_up_threads () {
    auto it = wake_ups.begin();
    for (unsigned i = 0; i < std::min (queue_t.size(), wake_ups.size()); i++) {
      (*it) (true);
      it++;
    }
  }

  virtual void on_queue_empty () {};
  virtual void on_queue_full () {};

public:
  asyncDispatcher () :
    dispatcher_enabled (true),
    buffer_size (1),
    working_threads (0)
  {}

  void set_buffer_size (unsigned r) { buffer_size = std::max (r, unsigned(1)); }

  unsigned queue_size () { return queue_t.size(); }

  void set_initial_queue (const std::queue<T>& q) {
    queue_t = q;
  }

  bool push_to_queue (T element) {

    if (queue_t.size() >= buffer_size) {
      on_queue_full();
      return false;
    }

    _dispatcher_mtx.lock();
      queue_t.push (element);
    _dispatcher_mtx.unlock();

    wake_up_threads();
    return true;
  }

  void start_dispatching (unsigned n_threads) {
    for (unsigned i = 0; i < n_threads; i++) {
      wake_ups.push_back (asyncManager::get_instance().add_persistent_sleepy_function(
        std::bind (&asyncDispatcher::try_dispatch, this)
      ));
    }
  }

  inline void pause_dispatching () {
    dispatcher_enabled = false;
  }

  inline void resume_dispatching () {
    dispatcher_enabled = true;
    wake_up_threads();
  }

  inline bool dispatching () {
    return dispatcher_enabled;
  }

  virtual void operation (T) = 0;
};

#endif // LIB_ASYNC_H
