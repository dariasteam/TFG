#include "../websearch.cpp"

extern "C" WebSearch* create_object()
{	
  return new WebSearch();
}

extern "C" void destroy_object( WebSearch* object )
{
  delete object;
}
