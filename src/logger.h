/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_H
#define LOGGER_H


#include <fstream>
#include <string>
#include <chrono>
#include <ctime>
#include <ratio>
#include <iostream>
#include <iomanip>
#include <queue>

#include "lib_async/lib_asynchronous.hpp"

#include "crawler.h"
#include "urlparser.h"
#include "htmlparser.h"
#include "reverseindex.h"
#include "ngram.h"

enum LogsPriority {
  NORMAL,
  WARNING,
  ERROR,
  FATAL
};

struct LogEntry {
  std::string message;
  std::string time_stamp;


  LogEntry (const std::string& m) {
    std::chrono::system_clock::time_point today =
    std::chrono::system_clock::now();

    time_t tt;
    tt = std::chrono::system_clock::to_time_t ( today );

    time_stamp = std::string (ctime(&tt));

    message = m;
  }
};

/**
 * @todo write docs
 */
class Logger : public asyncObject {
private:
  std::queue<LogEntry> event_queue;

  ReverseIndex* index;
  Crawler* crawler;
  URLParser* url_parser;
  HTMLParser* html_parser;


  std::string filename;
  std::ofstream file;

  unsigned delay_time;
  bool log ();

  bool alive;
public:
  /**
    * Default constructor
    */
  Logger(ReverseIndex* ind, Crawler* craw,
          HTMLParser* html, URLParser* url, NGram* ngram);

  inline void set_delay_time (unsigned t ) { delay_time = t; }
  inline void set_filename (std::string s ) { filename = s; }

  void start ();
  void stop ();

  void push_to_queue (LogEntry entry);

};

#endif // LOGGER_H
