/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "websearch.h"

WebSearch::WebSearch() :

  ngram (DEFAULT_N_NGRAM),
  html_parser (&reverse_index, &ngram),
  url_parser (&reverse_index, &page_rank),
  crawler (&url_parser, &html_parser, &reverse_index),
  logger (&reverse_index, &crawler, &html_parser, &url_parser, &ngram),
  engine (&reverse_index, &page_rank),
  ui (&reverse_index, &crawler, &html_parser,
               &url_parser, &engine, &ngram)
  {}

bool WebSearch::load_configuration_file(const std::string& config_file) {
  json::Parser parser;
  json::JsonTree jsontree;

  auto result = parser.parseFile (config_file, jsontree, false);

  if (result == json::OK) {

    unsigned queue_urlparser    = DEFAULT_QUEUE_SIZE;
    unsigned queue_htmlparser   = DEFAULT_QUEUE_SIZE;
    unsigned queue_crawler      = DEFAULT_QUEUE_SIZE;
    unsigned queue_reverseindex = DEFAULT_QUEUE_SIZE;
    unsigned queue_ngram        = DEFAULT_QUEUE_SIZE;

    unsigned n_ngram = DEFAULT_N_NGRAM;

    std::map<std::string, unsigned> n_threads;
    std::map<std::string, unsigned> queues_max_sizes;

    bool b = true;

    b &= jsontree.get (n_threads_urlparser,    "n_threads.urlparser");
    b &= jsontree.get (n_threads_htmlparser,   "n_threads.htmlparser");
    b &= jsontree.get (n_threads_crawler,      "n_threads.crawler");
    b &= jsontree.get (n_threads_reverseindex, "n_threads.reverseindex");
    b &= jsontree.get (n_threads_ngram,        "n_threads.ngram");

    b &= jsontree.get (queue_urlparser,    "queues_max_sizes.urlparser");
    b &= jsontree.get (queue_htmlparser,   "queues_max_sizes.htmlparser");
    b &= jsontree.get (queue_crawler,      "queues_max_sizes.crawler");
    b &= jsontree.get (queue_reverseindex, "queues_max_sizes.reverseindex");
    b &= jsontree.get (queue_ngram,        "queues_max_sizes.ngram");

    b &= jsontree.get (n_ngram, "ngram");

    if (!b) { std::cerr << "Error cargando la configuración\n"; return false;}

    ngram.set_buffer_size         (queue_ngram);
    url_parser.set_buffer_size    (queue_urlparser);
    html_parser.set_buffer_size   (queue_htmlparser);
    crawler.set_buffer_size       (queue_crawler);
    reverse_index.set_buffer_size (queue_reverseindex);
    page_rank.set_buffer_size     (10);

    return true;
  } else if (result == json::CANT_OPEN_FILE) {
    std::cerr << "El fichero de configuración no existe\n"
              << "Se ha generado uno nuevo en 'config.json'\n";
    DefaultFilesGenerator::get_instance().generate_config();
  } else if (result & (json::ERRORS | json::WARNINGS)) {
    std::cerr << "El fichero de configuración está mal formateado\n";
  }
  return false;
}

bool WebSearch::load_seed_file(const std::string& seed_file) {
  json::Parser parser;
  json::JsonTree jsontree;

  auto result = parser.parseFile (seed_file, jsontree, false);

  std::queue<std::string> default_seed ({
    "http://microsiervos.com/",
    "https://www.xataka.com/",
    "https://lamiradadelreplicante.com/",
    "http://www.rusadas.com/",
    "http://www.larealidadestupefaciente.com/"
  });

  if (result == json::OK) {
    std::vector <std::string> urls;

    bool b = true;

    b &= jsontree.get (urls, "seed");

    if (!b) { std::cerr << "Error cargando la semilla\n";}

    std::queue<std::string> seed;

    for (const auto& element : urls)
      seed.push (element);

    crawler.set_initial_queue(seed);

    return true;
  } else if (result == json::CANT_OPEN_FILE) {
    std::cerr << "El fichero de semilla no existe\n"
              << "Se ha generado una nueva en 'seed.json'\n";
    DefaultFilesGenerator::get_instance().generate_seed();
  } else if (result & (json::ERRORS | json::WARNINGS)) {
    std::cerr << "El fichero de semilla está mal formateado\n";
  }

  crawler.set_initial_queue(default_seed);

  return false;
}

bool WebSearch::prepare(const std::string& config_file,
                        const std::string& seed_file)
{
  bool b = true;
  b &= load_seed_file(seed_file);
  b &= load_configuration_file(config_file);

  return b;
}


void WebSearch::init() {
  ngram.start_dispatching         (n_threads_ngram);
  url_parser.start_dispatching    (n_threads_urlparser);
  html_parser.start_dispatching   (n_threads_htmlparser);
  crawler.start_dispatching       (n_threads_crawler);
  reverse_index.start_dispatching (n_threads_reverseindex);

  page_rank.start_dispatching     (1);
  logger.start();
  ui.start();

  asyncManager::get_instance().start();
}


WebSearch::~WebSearch() {
  logger.stop();
}
