/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DEFAULTFILESGENERATOR_H
#define DEFAULTFILESGENERATOR_H

#define DEFAULT_CONFIG_PATH "config.json"
#define DEFAULT_SEED_PATH "seed.json"
#define DEFAULT_HTML_PARSER_PATH "htmlparser_config.json"

#include <string>
#include <fstream>

/**
 * @todo write docs
 */
class DefaultFilesGenerator{
private:
  std::ofstream file;

  DefaultFilesGenerator () {}

  const std::string default_config = R"(
{
  "n_threads" : {
    "crawler"      : 3,
    "urlparser"    : 1,
    "htmlparser"   : 1,
    "reverseindex" : 1,
    "ngram"        : 1
  },
  "queues_max_sizes" : {
    "urlparser"    : 100000,
    "htmlparser"   : 200000,
    "crawler"      : 100000,
    "reverseindex" : 100000,
    "ngram"        : 100000
  },
  "ngram" : 3
})";

  const std::string default_seed = R"(
{
  "seed" : [
    "http://microsiervos.com/",
    "https://www.xataka.com/",
    "https://lamiradadelreplicante.com/",
    "http://www.rusadas.com/",
    "http://www.larealidadestupefaciente.com/"
  ]
})";

  const std::string default_html = R"(
{
  "exclusions" : [
    ["head",  "*",  ["*"]],
    ["style", "*",  ["*"]],
    ["script", "*", ["*"]],
    ["button", "*",  ["*"]],
    ["form", "*",  ["*"]],
    ["input", "*",  ["*"]]
  ],
  "inclusions" : [
    ["html", "*", ["*"]]
  ],
  "void_elements" : [
    "meta",
    "link",
    "img"
  ]
})";

public:
  void generate_config (const std::string& config_file = DEFAULT_CONFIG_PATH);
  void generate_seed   (const std::string& seed_file  = DEFAULT_SEED_PATH);
  void generate_htmlparser_file (const std::string& htmlparser_file  = DEFAULT_HTML_PARSER_PATH);

  static DefaultFilesGenerator& get_instance() {
    static DefaultFilesGenerator instance;
    return instance;
  }
};

#endif // DEFAULTFILESGENERATOR_H
