/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crawler.h"

Crawler::Crawler(URLParser* urlparser,
                 HTMLParser* tokenparser,
                 ReverseIndex* ind) :
                  url_parser (urlparser),
                  html_parser (tokenparser),
                  index (ind),
                  counter (0) {

  // CONNECTS
  url_parser->send_urls = [&](std::unordered_set<std::string> urls) {
    on_add_urls(urls);
  };

  url_parser->try_to_resume_crawler = [&] {
    on_try_to_resume();
  };

  html_parser->try_to_resume_crawler = [&] {
    on_try_to_resume();
  };
}

void Crawler::operation(std::string url) {
  CURL *curl;

  curl = curl_easy_init();

  std::string buff;

  visited_urls.insert(url);
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buff);
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_perform(curl);             // return a error code
  curl_easy_cleanup(curl);             // free memory

  std::vector<std::string> tags{"some", "example", "tags"};
  auto readBuffer = std::make_shared<std::string>(buff);

  bool b = true;

  b &= url_parser->push_to_queue({readBuffer, url});
  b &= html_parser->push_to_queue({readBuffer, url});

  index->insert_document(url, tags);

  if (!b) {
    std::cout << "Crawler stopped" << std::endl;
    pause_dispatching();
  }

  counter++;
}

size_t Crawler::WriteCallback(void* contents, size_t size, size_t nmemb,
                              void* userp){
  ((std::string*)userp)->append((char*)contents, size * nmemb);
  return size * nmemb;
}

void Crawler::on_add_urls(std::unordered_set<std::string> urls) {
  for (auto& url : urls) {
    if (visited_urls.find(url) == visited_urls.end())
      this->push_to_queue (url);
  }
}

void Crawler::on_try_to_resume() {
  resume_dispatching();
}
