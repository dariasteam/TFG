/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "htmlparser.h"


bool HTMLParser::load_hmlparser_file(const std::string& htmlparser_file) {
  json::Parser parser;
  json::JsonTree jsontree;

  XMLInfo info;

  auto result = parser.parseFile (htmlparser_file, jsontree, false);

  if (result & (json::ERRORS | json::WARNINGS)) {
    std::cerr << "El fichero de configuración de htmlparser está mal formateado\n";
    return false;
  } else if (result == json::CANT_OPEN_FILE) {
    std::cerr << "El fichero de configuración no existe\n"
    << "Se ha generado uno nuevo en 'htmlparser_config.json'\n";
    DefaultFilesGenerator::get_instance().generate_htmlparser_file();
  }

  bool b = info.serializeIn(htmlparser_file, "");

  if (!b) {
    std::cerr << "Error cargando el fichero htmlparser\n";
    return false;
  }

  for (auto& inclusion : info.inclusion_conditions)
    xml_parser.add_inclusion_condition(inclusion.tag,
                                        inclusion.key,
                                        inclusion.values);

  for (auto& exclusion : info.exclusion_conditions)
    xml_parser.add_exclusion_condition(exclusion.tag,
                                        exclusion.key,
                                        exclusion.values);

  xml_parser.set_void_elements(info.void_elements);

/*
  xml_parser.add_inclusion_condition("html", "*", {"*"});

  xml_parser.add_exclusion_condition("head",   "*", {"*"});
  xml_parser.add_exclusion_condition("style",  "*", {"*"});
  xml_parser.add_exclusion_condition("script", "*", {"*"});
  xml_parser.add_exclusion_condition("a",  "href", {"*"});

  xml_parser.add_exclusion_condition("button",  "*", {"*"});
  xml_parser.add_exclusion_condition("form",  "*", {"*"});
  xml_parser.add_exclusion_condition("input",  "*", {"*"});


*/
  return true;
}



void HTMLParser::operation(ContainerHTMLParser element) {

  if (element.content.get()->size () == 0) return;

  xml_parser.operator () (*element.content.get());

  std::vector<std::string> tokens;
  std::string data = xml_parser.get_data();
  std::istringstream iss(data);

  //std::cout << data << std::endl;

  // tokenize
  for(std::string s; iss >> s; ) {
    std::transform(s.begin(), s.end(), s.begin(), ::tolower); // string to lower
    tokens.push_back(s);
  }

  //auto final_tokens = std::make_shared<std::vector<std::string>> (*tokens);

  const ContainerReverseIndex aux_container {tokens, element.url};
  reverse_index->push_to_queue(aux_container);

  // Añadir al ngrama
  if (delivering_to_ngram)
    delivering_to_ngram = ngram->push_to_queue (data);
}

void HTMLParser::on_queue_empty(){
  if (have_been_full) {
    try_to_resume_crawler ();
    to_logger_empty();
    have_been_full = false;
  }
}

void HTMLParser::on_queue_full() {
  if (!have_been_full) {
    have_been_full = true;
    to_logger_full ();
  }
}

void HTMLParser::on_resume_delivering_to_ngram() {
  delivering_to_ngram = true;
}
