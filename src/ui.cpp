/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui.h"

UI::UI(ReverseIndex* ind, Crawler* craw,
       HTMLParser* html, URLParser* url, SearchEngine* eng,
       NGram* ngr) :
  index (ind),
  crawler (craw),
  html_parser (html),
  url_parser (url),
  engine (eng),
  ngram (ngr),
  alive (true),
  can_print (true)
  {}

void UI::search() {
  std::vector<std::string> input_vec;

  std::cout << "Input: ";

  std::string input;

  std::getline(std::cin, input);
  std::getline(std::cin, input);

  // input
  std::transform(input.begin(), input.end(), input.begin(), ::tolower);
  std::istringstream iss(input);
  std::string token;
  while( iss >> token ) input_vec.push_back(token);

  auto results = engine->search(input_vec);

  std::cout << "\n\n\n" <<
  "-------------RESULTS: ------------" << std::endl;

  unsigned max_size = 10;
  unsigned res_size = results.size();
  for (unsigned j = 0; j < std::min (res_size, max_size); j++) {
    std::cout << results[j].url << " "
    << results[j].token_weigth << " "
    << results[j].rank << std::endl;
  }

  std::string final_string = input;

  std::cout << "\n\n\n" <<
  "----------DID YOU MEAN?---------" << std::endl;

  unsigned counter = 0;

  auto sgst = ngram->operator() (input_vec);

  while (sgst.prob > 1e-10 && counter < 50) {
    final_string.append (" " + sgst.token);
    input_vec.push_back (sgst.token);
    counter++;
    sgst = ngram->operator() (input_vec);
  }

  std::cout << final_string << " (" << sgst.prob << ")" << std::endl;

  std::cout << "\n\n\n";
}


bool UI::master_handler(std::string input) {

  can_print = false;

  if (input == "0") {
    search();
  } else if (input == "q") {
    asyncManager::get_instance().end_unsafe();
    return false;
  }

  can_print = true;

  return true;
}

bool UI::wait_for_user_input () {
  std::string user_input;
  std::cin >> user_input;
  alive = master_handler (user_input);
  return alive;
}

void UI::show_header () {
  std::cout
  << "[0] Realizar búsqueda"<< "\n"
  << "[q] Salir"<< "\n\n";

  std::cout << std::setw(15) << std::left << "Crawler"
            << std::setw(15) << std::left << "URL Parser"
            << std::setw(15) << std::left << "HTML Parser"
            << std::setw(15) << std::left << "Reverse Index"
            << std::setw(15) << std::left << "NGram"
            << std::setw(15) << std::left << "n_records"
            << std::endl;
}

bool UI::menu_loop() {

  while (!can_print)
    std::this_thread::sleep_for(std::chrono::milliseconds(20));

  std::cout << "\r" << " "
            << std::setw(15) << std::left << crawler->queue_size()
            << std::setw(15) << std::left << url_parser->queue_size()
            << std::setw(15) << std::left << html_parser->queue_size()
            << std::setw(15) << std::left << index->queue_size()
            << std::setw(15) << std::left << ngram->queue_size()
            << std::setw(15) << std::left << index->get_n_documents()
            << std::flush;

  std::this_thread::sleep_for(std::chrono::milliseconds(20));

  return alive;
}

void UI::start () {
  show_header();
  asyncManager::get_instance().add_persistent_function(std::bind(&UI::wait_for_user_input, this));
  asyncManager::get_instance().add_persistent_function(std::bind(&UI::menu_loop, this));
}
