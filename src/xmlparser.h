/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XMLParser_H
#define XMLParser_H

#include <fstream>
#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <regex>
#include <sstream>
#include <functional>
#include <set>
#include <iostream>

enum InclusionCriteria {
  REGULAR,
  EXPLICITLY_EXCLUDED,
  EXPLICITLY_INCLUDED
};

struct PropertyCondition {
  std::unordered_multimap <std::string, std::vector<std::string>> condition;
};

/**
 * Allows retrieving content from a xml based string as plain text.
 * You can specify the desirable content by telling the tags, keys
 * and values to detect using the function @add_inclusion_condition
 * */
class XMLParser {
private:

  std::unordered_multimap <std::string, PropertyCondition> inclusion_conditions;
  std::unordered_multimap <std::string, PropertyCondition> exclusion_conditions;

  std::string data;
  unsigned nest_level;
  bool collecting;

  std::string current_tag;
  std::string current_key;
  std::string current_value;

  bool explicitly_excluded;

  // list of xml tags that don't require / when closing (/>)
  std::set <std::string> void_elements;

  bool check_tag (std::string tag) {
    return (inclusion_conditions.find(tag) != inclusion_conditions.end());
  }

  unsigned check_collect ();

  bool check_in_map (std::unordered_multimap <std::string, PropertyCondition>&
  map, std::string tag, std::string key, std::string value);

  // Handle '\n', ' ', '\t'
  void ignore_blanks (const std::string& document, int& index);

  // Handle comments
  void ignore_comments (const std::string& document, int& index);

  // Handle end tag </ada>
  bool parse_document_F (const std::string& document, int& index);

  // Obtains the value of a key
  bool parse_document_D (const std::string& document, int& index);

  // Obtains the a key of a tag
  bool parse_document_C (const std::string& document, int& index, bool collect);

  // Obtains the name of the tag
  // returns true if tag is closed
  bool parse_document_B (const std::string& document, int& index, bool collect);

  // Obtains the content between two tags
  bool parse_document_S (const std::string& document, int& index, bool collect);

public:
/** @brief Specify which tags, keys and values contain the data.
  *
  *  Sintax:
  *
  * - add_inclusion_condition("head",
  *                           "value",
  *                           {"2", "3"});
  *
  *   <head value = "2">   CAPTURED
  *   <head value = "3">   CAPTURED
  *   <head value = "1">   IGNORED
  *   <head other = "a">   IGNORED
  *   <head>               IGNORED
  *   <other>              IGNORED
  *
  * - add_inclusion_condition("head",
  *                           "value",
  *                            {"*"});
  *
  *   <head value = "2">   CAPTURED
  *   <head value = "3">   CAPTURED
  *   <head value = "1">   CAPTURED
  *   <head other = "a">   IGNORED
  *   <head>               IGNORED
  *   <other>              IGNORED
  *
  * - add_inclusion_condition("head",
  *                           "*",
  *                           {});
  *
  *   <head value = "2">   CAPTURED
  *   <head value = "3">   CAPTURED
  *   <head value = "1">   CAPTURED
  *   <head other = "a">   CAPTURED
  *   <head>               CAPTURED
  *   <other>              IGNORED
  *
  * - add_inclusion_condition("*",
  *                           "",
  *                           {});
  *
  *   <head value = "2">   CAPTURED
  *   <head value = "3">   CAPTURED
  *   <head value = "1">   CAPTURED
  *   <head other = "a">   CAPTURED
  *   <head>               CAPTURED
  *   <other>              CAPTURED
  *
  * NOTE: You can combine multiple conditions but be aware that using *
  *       will override all conditions in same level:
  *
  * - add_inclusion_condition("head", "*", {});
  * - add_inclusion_condition("head", "other", {"2"});
  *
  *   <head value = "2">   CAPTURED
  *   <head other = "3">   CAPTURED
  *   <head>               CAPTURED
  *
  * NOTE: Elements inherit conditions from its parent. Once a combinination
          is marked as included all it's children are also included.
          If you don't want this, you need to
          explicitly exclude them using @add_exclusion_condition
  *
  * - add_inclusion_condition("head", "value", {"1"});
  *
  *   <head value = "1">      CAPTURED
  *     <element other = "3"> CAPTURED
  *     </element>
  *   </head>
  *
  *   <element other = "3">   IGNORED
  *
  *
  * NOTE: If there is a circumstance in which certain combination is both
  *       explicitly included and excluded, the exclusion wins
  *
  * @param tag p_tag: tag
  * @param key p_key: key
  * @param values p_values: value
  */
  void add_inclusion_condition (std::string tag, std::string key,
                                std::vector<std::string> values);


  /**
  * @brief Specify which tags, keys and values must be excluded
  *
  * Use the same sintax of @add_inclusion_condition
  *
  * @param tag p_tag: tag
  * @param key p_key: key
  * @param values p_values: value
  */
  void add_exclusion_condition (std::string tag, std::string key,
                                std::vector<std::string> values);

  const std::string& get_data () { return data; }

  void set_void_elements (const std::vector<std::string>& voids) {
    for (auto& element : voids) {
      void_elements.insert (element);
    }
  }


  void operator () (const std::string& content);

  XMLParser () :
    nest_level (0),
    collecting (false),
    explicitly_excluded (false)
  {}

};

#endif // XMLParser_H
