/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef REVERSEINDEX_H
#define REVERSEINDEX_H

#include <iostream>
#include <cassert>
#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <sstream>
#include <cctype>
#include <fstream>
#include <mutex>
#include <algorithm>
#include <regex>

#include <stdio.h>
#include <string.h>

#include <leveldb/db.h>

#include "lib_async/lib_asynchronous.hpp"

struct WebDocument {
  std::vector<std::string> header_tags;
  unsigned long n_tokens;

  // [N_ELEMENTS][FIRST_ELEMENT_SZ][.....ELEMENT_1....][SECOND_ELEMENT_SZ]...
  std::string serialize_vector_out () const {
    size_t sz_acumuator = sizeof (header_tags.size());
    for (const auto& tag : header_tags) {
      sz_acumuator += sizeof (tag.size());
      sz_acumuator += tag.size() * sizeof (char);
    }

    char* v = new char [sz_acumuator];
    size_t offset = 0;

    // N_ELEMENTS
    size_t tags_sz = header_tags.size();
    memcpy(v + offset, &tags_sz, sizeof (tags_sz));
    offset += sizeof (tags_sz);

    // Each string
    for (const auto& tag : header_tags) {
      // ELEMENT_SZ
      size_t element_sz = tag.size();
      memcpy(v + offset, &element_sz, sizeof (element_sz));
      offset += sizeof(element_sz);

      // ELEMENT_CONTENT
      strncpy(v + offset, tag.c_str(), tag.size());
      offset += tag.size();
    }

    std::string result (v, v + sz_acumuator);
    return result;
  }

  // [N_TOKENS][VECTOR....]
  std::string serialize_out () const {
    std::string v = serialize_vector_out();

    //size_t url_sz = url.size();
    size_t total_sz = sizeof (n_tokens);

    char* c = new char [total_sz];

    size_t offset = 0;

    // N_TOKENS
    memcpy(c + offset, &n_tokens, sizeof (n_tokens));
    offset += sizeof (n_tokens);

    // VECTOR
    std::string result (c, c + total_sz);
    result.append(v);

    delete [] c;
    return result;
  }

  // [N_TOKENS][VECTOR....]
  bool serialize_in(const std::string& content) {
    const char* t = content.c_str();
    size_t offset = 0;

    // N_TOKENS
    memcpy(&n_tokens, t + offset, sizeof (n_tokens));
    offset += sizeof (n_tokens);

    // N_ELEMENTS
    size_t n_elements = header_tags.size();
    memcpy(&n_elements, t + offset, sizeof (n_elements));
    offset += sizeof (n_elements);

    // For each token
    for (size_t i = 0; i < n_elements; i++) {
      // ELEMENT_SZ
      std::string dummy_s;
      size_t sz_2 = dummy_s.size();
      memcpy(&sz_2, t + offset, sizeof (sz_2));
      offset += sizeof (sz_2);

      // ELEMENT_CONTENT
      char* aux_c = new char [sz_2];

      strncpy(aux_c, t + offset, sz_2);;
      offset += sz_2;

      std::string a (aux_c);
      delete [] aux_c;

      header_tags.push_back(a);
    }
    return true;
  }
};

struct IndexEntry {
  unsigned n_ocurrences;

  std::string serialize_out() const {
    char* n = new char [sizeof (n_ocurrences)];
    memcpy(n, &n_ocurrences, sizeof (n_ocurrences));
    std::string s (n, sizeof (n_ocurrences));
    delete [] n;
    return s;
  }

  void serialize_in (const std::string& s) {
    const char* c = s.c_str();
    memcpy(&n_ocurrences, c, sizeof (n_ocurrences));
  }
};

struct ContainerReverseIndex {
  std::vector<std::string> tokens;
  std::string url;
};

struct IndexResult {
  std::string url;
  double weigth;
};


class DocumentStorage {
  std::mutex mtx;
public:
  leveldb::DB* db_documents;

  DocumentStorage () {
    leveldb::Options options;
    options.create_if_missing = true;
    leveldb::Status status = leveldb::DB::Open(options, "/tmp/db_documents", &db_documents);
    assert(status.ok());
  }

  ~DocumentStorage () {
    delete db_documents;
  }

  bool store (const std::string& url, const WebDocument d) {
    std::string content = d.serialize_out();
    leveldb::Slice s2 = content;
    mtx.lock();
      db_documents->Put(leveldb::WriteOptions(), url, s2);
    mtx.unlock();
    return true;
  }

  bool retrieve(const std::string& url, WebDocument& result) {
    std::string receiver;
    mtx.lock();
      auto status  = db_documents->Get(leveldb::ReadOptions(), url, &receiver);
    mtx.unlock();
    result.serialize_in(receiver);

    if(!status.ok()) {
      std::cerr << "Error, no existe entrada en la base " <<
      "de datos para la url visitada" << url << std::endl;
      std::cerr << status.ToString() << std::endl;
    }

    return status.ok();
  }
};

class IndexStorage {
  std::mutex mtx;
public:
  bool store (const std::string& token,
              const std::pair<std::string, IndexEntry>& p) {

    mtx.lock();
      leveldb::DB* db_token;
      leveldb::Options options;
      options.create_if_missing = true;
      std::string db_name ("/tmp/db_token_");
      db_name.append(token);
      leveldb::Status status = leveldb::DB::Open(options, db_name, &db_token);

      if (!status.ok())
        std::cerr << status.ToString() << std::endl;
      assert(status.ok());

      db_token->Put(leveldb::WriteOptions(), p.first, p.second.serialize_out());
      delete db_token;
    mtx.unlock();

    return status.ok();
  }

  bool retrieve(const std::string& token, std::unordered_map<std::string, IndexEntry>& aux_map) {
    mtx.lock();

    leveldb::DB* db_token;
    leveldb::Options options;
    std::string db_name ("/tmp/db_token_");
    db_name.append(token);
    leveldb::Status status = leveldb::DB::Open(options, db_name, &db_token);

    if (!status.ok()) {
      mtx.unlock();
      return false;
    }

    auto it = db_token->NewIterator(leveldb::ReadOptions());
    for (it->SeekToFirst(); it->Valid(); it->Next()) {
        IndexEntry entry;
        entry.serialize_in (it->value().ToString());
        aux_map[it->key().ToString()] = entry;
    }
    if (false == it->status().ok()) {
        std::cerr << "An error was found during the scan" << std::endl;
        std::cerr << it->status().ToString() << std::endl;
        delete db_token;
        return false;
    }
    delete db_token;

    mtx.unlock();
    return true;
  }
};


/**
 * @todo write docs
 */
class ReverseIndex : public asyncDispatcher<ContainerReverseIndex> {
private:
  unsigned long entries {0};    // should be initialized with db size

  // Hash de url / web document
  DocumentStorage documents;

  // Índice inverso real token -> { url, indexEntry}
  /*
  std::unordered_map<std::string,
                     std::unordered_map<std::string, IndexEntry>> index;
                     */
  IndexStorage index;

  void operation (ContainerReverseIndex element);

public:
  //void export_token_list (std::string filename);

  //unsigned get_n_documents () { return documents.size(); }

  ReverseIndex () {}

  void insert_document (std::string url, std::vector<std::string> headers) {
    documents.store(url, WebDocument{headers, 0});
  }

  unsigned long get_n_documents() {
    return entries;
  }

  std::vector<IndexResult> search (const std::vector<std::string> input);

  ~ReverseIndex ();
};

#endif // REVERSEINDEX_H
