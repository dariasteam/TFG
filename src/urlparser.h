/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef URLPARSER_H
#define URLPARSER_H

#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <sstream>
#include <memory>

#include "reverseindex.h"
#include "urlnormalizer.h"
#include "pagerank.h"

#include "lib_async/lib_asynchronous.hpp"

struct ContainerURLParser {
  std::shared_ptr<std::string> content;
  std::string url;
};

/**
 * @todo write docs
 */
class URLParser : public asyncDispatcher<ContainerURLParser> {
private:
  std::regex HREF;
  std::regex HTTP;
  std::regex EXTENSION;

  ReverseIndex* index;
  PageRank* page_rank;

  URLNormalizer url_normalizer;

  // Check url is http / https
  bool is_http_url (const std::string& url) { return std::regex_search(url, HTTP); }
  bool is_html_url (const std::string& url) { return !std::regex_search(url, EXTENSION); }

  void extract_urls (std::shared_ptr<std::string> content,
                     std::unordered_set<std::string>& set);

  bool have_been_full;

  void on_queue_empty();
  void on_queue_full();
public:

  void operation (ContainerURLParser element);

  URLParser (ReverseIndex* ind, PageRank* pag) :
    HREF ("href=\"(.+?)\""),
    HTTP ("^https?.*$"),
    EXTENSION ("^.*\\.(png|jpg|css|tiff|js|pdf)$"), // skip .
    index (ind),
    page_rank (pag),
    have_been_full (false)
  {}

  // SIGNAL
  std::function<void (std::unordered_set<std::string> urls)> send_urls;
  std::function<void (void)> try_to_resume_crawler;
  std::function<void (void)> to_logger_full;
  std::function<void (void)> to_logger_empty;

};

#endif // URLPARSER_H
