/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reverseindex.h"

void ReverseIndex::operation(ContainerReverseIndex element) {

  // Impide condiciones de carrera en las que se accede a objetos documentos
  // antes de que existan

  WebDocument doc;
  bool exist = documents.retrieve(element.url, doc);
  if (exist) {
    doc.n_tokens = element.tokens.size();
  } else {
    push_to_queue(element);
    return;
  }

  documents.store(element.url, doc);
  entries++;

  // buscar por si existe el token
  for (const auto& token : (element.tokens)) {

    std::regex r("[a-zA-Z]+");
    if (std::regex_match (token,r)) {

      std::unordered_map<std::string, IndexEntry> map;
      bool exist = index.retrieve(token, map);

      std::pair<std::string, IndexEntry> pair;
      if (!exist) {                   // No existe el token en el índice
        pair = std::make_pair (element.url, IndexEntry{1});
      } else {                        // Si existe el token en el índice
        auto& doc_entry = map;
        const auto& it2 = doc_entry.find(element.url);

        if (it2 == doc_entry.end()) { // La web nunca se ha registrado
          pair = std::make_pair (element.url, IndexEntry{1});
        } else {                      // La web ya había sido registrada
          pair = std::make_pair (element.url, IndexEntry{(*it2).second.n_ocurrences += 1});
        }
      }

      index.store(token, pair);

    }
  }

}

std::vector<IndexResult> ReverseIndex::search
                                        (const std::vector<std::string >input) {



  std::unordered_map<std::string, double> docs_ocurrences;

  // Buscar coincidencias con los tokens e insertar en docs_ocurrences
  for (const auto& token : input) {
    std::unordered_map<std::string, IndexEntry> map;
    bool exist = index.retrieve(token, map);

    if (exist) {             // existe el token en la base de datos
      const auto& doc_entry = map;
      for (const auto& element : doc_entry) { // recuperar todas las webs
        unsigned ocurrences = element.second.n_ocurrences;
        std::string url = element.first;
        WebDocument webDoc;
        bool exist = documents.retrieve(element.first, webDoc);

        if (!exist) { return {}; }

        unsigned n_tokens = webDoc.n_tokens;

        if (docs_ocurrences.find(url) == docs_ocurrences.end()) {
          docs_ocurrences[url] = double(ocurrences) / n_tokens;
        } else {
          docs_ocurrences[url] += double(ocurrences) / n_tokens;
        }
      }
    }
  }

  std::vector<IndexResult> results (docs_ocurrences.size());

  // Introducir los resultados en un vector
  unsigned i = 0;
  for (const auto& element : docs_ocurrences) {
    results[i] = {element.first, element.second};
    i++;
  }

  return results;
}

ReverseIndex::~ReverseIndex () {

}
