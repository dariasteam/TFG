/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NGRAM_H
#define NGRAM_H

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <cmath>
#include <string>
#include <mutex>
#include <atomic>
#include <algorithm>

#include "lib_async/lib_asynchronous.hpp"

/**
 * @todo write docs
 */
struct prediction {
  std::string token;
  double prob;
};

/**
 * @todo write docs
 */
class NGram : public asyncDispatcher<std::string> {
private:

  struct NGramContainer {
    unsigned n_ocurrences;
    double prob;
    std::map <std::string, NGramContainer*>  content;
    ~NGramContainer() {
      for (auto& element : content)
        delete element.second;
    }
  };

  NGramContainer tokens_tree;

  std::vector<std::vector<std::string>> corpus;
  std::set <std::string> vocabulary;

  unsigned unseen_tokens_counter;
  unsigned n_tokens_in_corpus;
  unsigned N;
  double unseen_probability;

  static std::mutex mtx;

  inline bool is_separator (const char c) const {
    return (c == ' ' || c == '\n' || c == ','  || c == '\\' || c == '/' ||
    c == '+' || c == '-'  || c == '*'  || c == '['  || c == ']' ||
    c == '(' || c == ')'  || c == '\t' || c == '.'  || c == '?' ||
    c == '!' || c == '&');
  }

  inline bool is_end_of_line (const char c) const {
    return (c == '.' || c == '?' || c == '!');
  }

  inline void insert_token (const std::vector<std::string>& tokens);
  std::string get_random_token();

  void calculate_probabilities_recursive (NGramContainer* node, unsigned ac);
  void calculate_probabilities ();

  std::vector<std::string> get_substr_tokens (const std::vector<std::string>&
                                              tokens, unsigned i, unsigned j);

  double get_probability_recursive (NGramContainer* node,
                                    const std::vector<std::string>& tokens);

  void calculate_ocurrences ();
  void parse (const std::string& content);

  bool have_been_full;

  void operation (std::string data)  {
    load_corpus (data);
  }

  void on_queue_empty ();
  void on_queue_full ();

public:

  void load_corpus (const std::string& text);
  prediction operator () (const std::vector<std::string>& tokens);

  NGram (unsigned n);

  unsigned get_vocabulary_sz () { return vocabulary.size(); }
  double get_probability (const std::vector<std::string>& tokens);

  // SIGNAL
  std::function<void (void)> try_to_resume_delivering;
  std::function<void (void)> to_logger_full;
  std::function<void (void)> to_logger_empty;

};

#endif // NGRAM_H
