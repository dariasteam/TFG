/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ngram.h"

std::mutex NGram::mtx;

void NGram::insert_token(const std::vector<std::string>& tokens) {

    unsigned size = tokens.size();

    NGramContainer* previous_obj = &tokens_tree;

    mtx.lock();
      previous_obj->n_ocurrences++;
    mtx.unlock();

    std::map <std::string, NGramContainer*>::iterator it;

    for (unsigned i = 0; i < size; i++) {
      it = previous_obj->content.find (tokens[i]);
      if (it != previous_obj->content.end()) {             // existe el token
        previous_obj = (*it).second;
      } else {                                             // no existe el token
        NGramContainer* aux_obj = new NGramContainer;
        mtx.lock();
          previous_obj->content.insert (std::make_pair(tokens[i], aux_obj));
        mtx.unlock();
        previous_obj = aux_obj;
      }
      mtx.lock();
        previous_obj->n_ocurrences++;
      mtx.unlock();
    }
  }

void NGram::calculate_probabilities_recursive(NGramContainer* node,
                                              unsigned int ac) {

    unsigned aux_ac = node->n_ocurrences;
    double A = double(ac + aux_ac);
    double B = ac;

    mtx.lock();
      node->prob = log(A / B);    // FIXME: this should not use a mutex
    mtx.unlock();

    for (auto& sub_element : node->content)
      calculate_probabilities_recursive(sub_element.second, aux_ac);
}


void NGram::calculate_probabilities() {
  calculate_probabilities_recursive (&tokens_tree, 1);

  double C = unseen_tokens_counter + 1;
  double N = n_tokens_in_corpus;
  double V = vocabulary.size();

  mtx.lock();
    unseen_probability = log (C / (N + V));
  mtx.unlock();
}

double NGram::get_probability_recursive(NGramContainer* node,
                                        const std::vector<std::string>& tokens){

  if (tokens.size() == 0)
    return node->prob;

  auto container = node->content;
  auto it = container.find (tokens[0]);
  auto aux = get_substr_tokens(tokens, 1, tokens.size() - 1);

  if (it != container.end()) {
    return node->prob * get_probability_recursive ((*it).second, aux);
  } else {
    return unseen_probability;
  }
}

std::vector< std::string > NGram::get_substr_tokens(
  const std::vector<std::string>& tokens,
  unsigned int i, unsigned int j) {
  std::vector<std::string> aux;
  for (unsigned k = i; k <= j; k++)
    aux.push_back(tokens[k]);

  return aux;
  }

void NGram::calculate_ocurrences() {
  for (const auto& line : corpus) {
    // Regular tokens
    for (unsigned i = 0; i + N - 1 < line.size(); i++) {
      auto aux_tokens = get_substr_tokens (line, i, i + N - 1);
      insert_token (aux_tokens);
    }
  }
}

void NGram::parse(const std::string& content) {
  unsigned i_0 = 0;

  std::vector<std::string> aux_vec;
  for (unsigned i = 0; i < content.size(); i++) {
    if (is_separator(content[i])) {
      std::string token = content.substr(i_0, i - i_0);

      std::transform(token.begin(), token.end(), token.begin(), ::tolower);

      aux_vec.push_back (token);

      mtx.lock();

      vocabulary.insert (token);
      n_tokens_in_corpus++;

      if (is_end_of_line(content[i])) {
        corpus.push_back(aux_vec);
        aux_vec.clear();
      }

      mtx.unlock();

      while (is_separator(content[i]))
        i++;

      i_0 = i;
    }
  }
}

prediction NGram::operator()(const std::vector<std::string >& tokens) {
  std::string best_string; // = get_random_token();
  double best_val = unseen_probability;

  NGramContainer* obj = &tokens_tree;
  std::map <std::string, NGramContainer*>::iterator it;

  std::string last_token;

  unsigned from = std::max(static_cast<int>(tokens.size()) - int(N - 1), 0);
  unsigned to = tokens.size();

  for (unsigned i = from; i < to; i++) {
    auto aux_content = obj->content;
    it = aux_content.find(tokens[i]);
    if (it != aux_content.end()) {
      obj = (*it).second;
      last_token = (*it).first;
    } else {
      break;
    }
  }

  if (obj == &tokens_tree) {
    return {best_string, best_val};
  }

  auto aux_content = obj->content;
  if (aux_content.size() > 0) {

    if (tokens.size() > 1) {
      std::vector<std::string> aux_vec;
      for (unsigned i = 1; i < tokens.size(); i++)
        aux_vec.push_back (tokens[i]);

      return operator () (aux_vec);
    } else {
      for (const auto& entry : aux_content) {
        if (entry.second->prob > best_val) {
          best_val = entry.second->prob;
          best_string = entry.first;
        }
      }
    }

  } else {
    best_val = obj->prob;
    best_string = last_token;
  }

  return {best_string, best_val};
}

NGram::NGram(unsigned int n)  :
    unseen_tokens_counter (0),
    n_tokens_in_corpus (0),
    N (n),
    have_been_full (false)
  {
    std::srand(time(NULL));
  }

double NGram::get_probability(const std::vector< std::string >&tokens) {
   return get_probability_recursive (&tokens_tree, tokens);
}

std::string NGram::get_random_token(){
  std::string token = "";
  while (token == "") {
    for (const auto& element : vocabulary) {
      if ((rand() % 1000 < 1)) {
        token = element;
        break;
      }
    }
  }
  return token;
}


void NGram::load_corpus(const std::string& text) {
  // FIXME: Should we empty the corpus?
  corpus.clear();
  parse (text);
  calculate_ocurrences();
  calculate_probabilities();
}


void NGram::on_queue_empty(){
  if (have_been_full) {
    to_logger_empty();
    have_been_full = false;
  }
}

void NGram::on_queue_full() {
  if (have_been_full == false) {
    have_been_full = true;
    to_logger_full ();
  }
}
