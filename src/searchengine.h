/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SEARCHENGINE_H
#define SEARCHENGINE_H

#include <string>
#include <iostream>
#include <vector>

#include "reverseindex.h"
#include "pagerank.h"

struct Result {
  std::string url;
  double token_weigth;
  double rank;
};

/**
 * @todo write docs
 */
class SearchEngine {
private:
  ReverseIndex* index;
  PageRank* page_rank;
public:

  SearchEngine (ReverseIndex* ind, PageRank* pag) :
    index (ind),
    page_rank (pag)
    {}

  std::vector<Result> search (const std::vector<std::string > input);
};

#endif // SEARCHENGINE_H
