/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef WEBSEACH_H
#define WEBSEACH_H

#include <vector>
#include <string>
#include <iostream>
#include <map>

#include "lib_async/lib_asynchronous.hpp"
#include "lib_jsoncpp/parser.hpp"

#include "crawler.h"
#include "htmlparser.h"
#include "urlparser.h"
#include "ui.h"
#include "reverseindex.h"
#include "ngram.h"
#include "pagerank.h"
#include "logger.h"
#include "defaultfilesgenerator.h"

#define DEFAULT_N_NGRAM 3
#define DEFAULT_QUEUE_SIZE 100
#define DEFAULT_THREADS 1


/**
 * @todo write docs
 */
class WebSearch {
private:

  unsigned n_threads_urlparser    = DEFAULT_THREADS;
  unsigned n_threads_htmlparser   = DEFAULT_THREADS;
  unsigned n_threads_crawler      = DEFAULT_THREADS;
  unsigned n_threads_reverseindex = DEFAULT_THREADS;
  unsigned n_threads_ngram        = DEFAULT_THREADS;

  NGram ngram;
  PageRank page_rank;
  ReverseIndex reverse_index;
  HTMLParser html_parser;
  URLParser url_parser;
  Crawler crawler;
  Logger logger;
  SearchEngine engine;
  UI ui;

  bool load_configuration_file (const std::string& config_file);
  bool load_seed_file (const std::string& seed_file);

public:
  WebSearch ();
  virtual bool prepare (const std::string& config_file = DEFAULT_CONFIG_PATH,
                const std::string& seed_file = DEFAULT_SEED_PATH);
  virtual void init ();
  ~WebSearch ();
};

#endif // WEBSEACH_H
