/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "urlparser.h"

void URLParser::extract_urls(std::shared_ptr<std::string> content,
                             std::unordered_set<std::string>& set) {
  std::smatch links_match;
  const auto words_begin = std::sregex_iterator(content->begin(), content->end(), HREF);
  const auto words_end = std::sregex_iterator();

  for (std::sregex_iterator i = words_begin; i != words_end; ++i) {

    std::smatch match = *i;
    std::ssub_match sub_match = match[1];
    std::string match_str = sub_match.str();

    if (is_http_url(match_str) && is_html_url(match_str)) {
      url_normalizer.normalize(match_str);  // normalizar
      set.insert(match_str);
    }
  }
}

void URLParser::operation(ContainerURLParser element) {
  std::unordered_set<std::string> extracted_urls;

  extract_urls(element.content, extracted_urls);
  send_urls (extracted_urls);

  page_rank->push_to_queue ({element.url, extracted_urls});
}

void URLParser::on_queue_empty() {
  //try_to_resume_crawler ();
  if (have_been_full) {
    std::cout << "EL url parser está vacío" << std::endl;
    try_to_resume_crawler ();
    have_been_full = false;
  }
}

void URLParser::on_queue_full() {
  if (have_been_full == false) {
    have_been_full = true;
    to_logger_full ();
  }
}

