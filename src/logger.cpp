/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "logger.h"

Logger::Logger(ReverseIndex* ind, Crawler* craw,
               HTMLParser* html, URLParser* url, NGram* ngram) :
               index (ind),
               crawler (craw),
               url_parser (url),
               html_parser (html),
               delay_time (1),
               alive (false)
    {
      std::chrono::system_clock::time_point today =
                   std::chrono::system_clock::now();

      time_t tt;
      tt = std::chrono::system_clock::to_time_t ( today );
      ctime(&tt);

      filename = "logs/WebSearch - " + std::string (ctime(&tt)) + ".log";



      // CONNECTS
      html_parser->to_logger_full = [&] {
        push_to_queue (LogEntry ("HTMLParser is full"));
      };

      html_parser->to_logger_empty = [&] {
        push_to_queue (LogEntry ("HTMLParser is empty"));
      };

      ngram->to_logger_full = [&] {
        push_to_queue (LogEntry ("Ngram is full"));
      };

      ngram->to_logger_empty = [&] {
        push_to_queue (LogEntry ("Ngram is empty"));
      };

      url_parser->to_logger_full = [&] {
        push_to_queue (LogEntry ("URLParser is full"));
      };

      url_parser->to_logger_empty = [&] {
        push_to_queue (LogEntry ("URLParser is empty"));
      };

    }

bool Logger::log() {
  file.open(filename, std::ios::app);

  while (event_queue.size() > 0) {
    auto entry = event_queue.front();
    event_queue.pop();

    file << entry.message << " " << entry.time_stamp;
  }

  file << "\n"
  << std::setw(15) << std::left << crawler->queue_size()
  << std::setw(15) << std::left << url_parser->queue_size()
  << std::setw(15) << std::left << html_parser->queue_size()
  << std::setw(15) << std::left << index->queue_size()
  << std::setw(15) << std::left << index->get_n_documents();

  file.close();

  std::this_thread::sleep_for(std::chrono::seconds(delay_time));
  return alive;
}

void Logger::start() {
  alive = true;
  asyncManager::get_instance().add_persistent_function(std::bind(&Logger::log,this));
}

void Logger::stop() {
  alive = false;
}

void Logger::push_to_queue(LogEntry entry) {
  event_queue.push (entry);
}
