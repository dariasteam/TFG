/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "xmlparser.h"

bool XMLParser::check_in_map (std::unordered_multimap <std::string,
                                                        PropertyCondition>& map,
                               std::string aux_tag, std::string aux_key,
                               std::string aux_value) {

  if (map.find("*") != map.end())
    return true;

  auto range = map.equal_range(aux_tag);

  for (auto it = range.first; it != range.second; it++) {

    auto key = (*it).second.condition;
    if (key.find("*") != key.end())
      return true;

    auto range_2 = key.equal_range(aux_key);

    for (auto it2 = range_2.first; it2 != range_2.second; it2++) {

      for (const auto& value : (*it2).second ) {
        if (value == aux_value || value == "*")
          return true;
      }
    }
  }

  return false;
}


unsigned XMLParser::check_collect () {

  bool included = check_in_map (inclusion_conditions, current_tag, current_key,
                                current_value);

  bool excluded = check_in_map (exclusion_conditions, current_tag, current_key,
                                current_value);

  if (current_tag == "div") {
   // std::cout << "DUVE -> in:" << included << " ex: " << excluded << std::endl;
  }

  if (excluded) {
    explicitly_excluded = true;
    return EXPLICITLY_EXCLUDED;
  } else if (included) {
    return EXPLICITLY_INCLUDED;
  } else {
    return REGULAR;
  }
}

void XMLParser::add_inclusion_condition (std::string tag, std::string key,
                               std::vector<std::string > values){
  PropertyCondition p;
  p.condition.insert(std::make_pair(key, values));
  inclusion_conditions.insert(std::make_pair(tag, p));
}

void XMLParser::add_exclusion_condition (std::string tag, std::string key,
                                        std::vector<std::string > values){
  PropertyCondition p;
  p.condition.insert(std::make_pair(key, values));
  exclusion_conditions.insert(std::make_pair(tag, p));
}

void XMLParser::operator()(const std::string& content){
  int index = -1;

  collecting = false;

  current_key.clear();
  current_tag.clear();
  current_value.clear();
  data.clear();

  if (content.size () > 0)
    parse_document_S(content, index, false);
}

void XMLParser::ignore_blanks (const std::string& document, int& index){
  for (index++; index < document.size() - 1; index++) {
    char character = document[index];
    if (character != ' ' && character != '\n' && character != '\t') {
      index--;
      return;
    }
  }
}

void XMLParser::ignore_comments (const std::string& document, int& index) {

  // Is a comment
  if (document[index+1] == '-' && document[index+2] == '-') {
    for (index++; index < document.size() - 2; index++) {
      if (document[index] == '-' && document[index + 1] == '-' &&
          document[index + 2] == '>') {
        index+=2;
        break;
      }
    }
  } else { // Is doctype
    for (index++; index < document.size() - 2; index++) {
      if (document[index] == '>')
        break;
    }
  }
}


bool XMLParser::parse_document_F (const std::string& document, int& index) {
  ignore_blanks(document, index);
  for (index++; index < document.size() - 1; index++) {
    char character = document[index];
    switch (character) {
      case '>':
        return false;
        break;
    }
  }
  return false;
}

bool XMLParser::parse_document_D (const std::string& document, int& index) {
  // recorrer hasta size - 1 para permitir el lookahead

  current_value.clear();
  ignore_blanks(document, index);
  for (index++; index < document.size() - 1; index++) {
    char character = document[index];
    switch (character) {
      case '"':
        return true;
        break;
      default:
        current_value.push_back (character);
        break;
    }
  }
  return false;
}

bool XMLParser::parse_document_C (const std::string& document, int& index,
                                  bool collect) {
  current_key.clear();
  ignore_blanks(document, index);

  bool aux_collect = false;
  unsigned inclusion_info = REGULAR;

  // recorrer hasta size - 1 para permitir el lookahead
  for (index++; index < document.size() - 1; index++) {
    char character = document[index];
    switch (character) {
      case '=':
        ignore_blanks(document, index);
        if (document[index + 1] == '"') {
          index++;
          parse_document_D (document, index);
        }

        if (!explicitly_excluded) {
          inclusion_info = check_collect();
          aux_collect = collect = (inclusion_info != EXPLICITLY_EXCLUDED);
        }

        current_key.clear();

        break;
      case '>':
        if (void_elements.find(current_tag) != void_elements.end()) {
          if (inclusion_info == EXPLICITLY_EXCLUDED) explicitly_excluded =false;
          return false;
        } else {
          aux_collect = parse_document_S(document, index, collect);
          if (inclusion_info == EXPLICITLY_EXCLUDED) explicitly_excluded =false;
          return aux_collect;
        }

        break;
      case '/':
        if (document[index + 1] == '>') {
          index++;
          if (inclusion_info == EXPLICITLY_EXCLUDED) explicitly_excluded =false;
          current_tag.clear();
          return false;
        }
        break;
      default:
        if (character != ' ' && character != '\n' && character != '\t')
          current_key.push_back (character);
        break;
    }
  }
  return false;
}

bool XMLParser::parse_document_B (const std::string& document, int& index,
                                  bool collect) {

  current_tag.clear();
  //current_key.clear() ;   // FIXME: Is this necessary?
  //current_value.clear();

  unsigned inclusion_info = REGULAR;
  bool aux_result;

  // recorrer hasta size - 1 para permitir el lookahead
  for (index++; index < document.size() - 1; index++) {
    char character = document[index];
    switch (character) {
      case '>':

        if (void_elements.find(current_tag) != void_elements.end()) {
          if (inclusion_info == EXPLICITLY_EXCLUDED)
            explicitly_excluded = false;
          return true;
        }

        if (!explicitly_excluded) {
          inclusion_info = check_collect();
          collect = (inclusion_info != EXPLICITLY_EXCLUDED);
        }

        aux_result = parse_document_S(document, index, collect);

        if (inclusion_info == EXPLICITLY_EXCLUDED) explicitly_excluded = false;

        return aux_result;
        break;
      case '/':
        if (document[index + 1] == '>') {
          index++;
          return true;
        }
        break;
      default:
        if (character == ' ' || character == '\n' || character == '\t') {
          return parse_document_C(document, index, collect);
        } else {
          current_tag.push_back(character);
        }
        break;
    }
  }
  return false;
}

bool is_word (char c) {
  std::string s ("0");
  s[0] = c;
  std::regex r ("[a-zA-Z]");

  return std::regex_match(s, r);
}

bool XMLParser::parse_document_S (const std::string& document, int& index,
                                  bool collect) {

  // recorrer hasta size - 1 para permitir el lookahead
  ignore_blanks(document, index);

  for (index++; index < document.size() - 1; index++) {
    char character = document[index];
    switch (character) {
      case '<':
        if (document[index + 1] == '/') {
          index++;
          return parse_document_F (document, index);
        } else if (document[index + 1] == '!') {
          index++;
          ignore_comments (document, index);
        } else if (is_word(document[index + 1])){
          if(parse_document_B (document, index, collect))
            return false;
        }
        break;
      default:
        if (collect)
          data.push_back (character);
        break;
    }
  }
  return false;
}
