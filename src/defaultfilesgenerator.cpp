/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "defaultfilesgenerator.h"


void DefaultFilesGenerator::generate_config(const std::string& config_file) {
  file.open (config_file);
  if (file.is_open())
    file << default_config;
  file.close();
}

void DefaultFilesGenerator::generate_seed(const std::string& seed_file) {
  file.open (seed_file);
  if (file.is_open())
    file << default_seed;
  file.close();;
}


void DefaultFilesGenerator::generate_htmlparser_file(const std::string& htmlparser_file) {
  file.open (htmlparser_file);
  if (file.is_open())
    file << default_html;
  file.close();;
}
