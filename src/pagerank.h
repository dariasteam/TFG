/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PAGERANK_H
#define PAGERANK_H

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <iostream>
#include <math.h>

#include "math/matrix.h"

#include "lib_async/lib_asynchronous.hpp"

struct ContainerPageRank {
  std::string base_url;
  std::unordered_set<std::string> linked_urls;
};

/**
 * @todo write docs
 *
 *
 * FIXME: No permitir enlaces desde el mismo dominio
 */
class PageRank : public asyncDispatcher<ContainerPageRank> {
private:
  std::unordered_map <std::string, unsigned long> url_to_index;
  std::vector<std::vector<unsigned>> links_matrix;

  Matrix weight_matrix;

  std::mutex mtx;

  void insert_url (const std::unordered_set<std::string>& urls);
  void operation (ContainerPageRank element);

  double c = 0.85;
  Gauss gauss;

public:
  PageRank () {}

  std::vector<double> get_rank (const std::vector<std::string>& urls);
};

#endif // PAGERANK_H
