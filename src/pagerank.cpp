/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pagerank.h"

// Inserta las nuevas URL en la tabla
void PageRank::insert_url(const std::unordered_set<std::string>& urls) {
  unsigned long old_size = links_matrix.size();
  unsigned long size = old_size;

  // Asigna un indice a las url
  for (const auto& url : urls) {
    url_to_index[url] = size;
    size++;
  }

  links_matrix.resize(size);
}

void PageRank::operation(ContainerPageRank element) {

  std::unordered_set <std::string> unknown_urls {element.base_url};
  std::unordered_set <std::string> known_urls;

  // Averiguar qué urls ya están en la tabla
  for (const auto& url : element.linked_urls) {
    auto it = url_to_index.find (url);
    if (it != url_to_index.end())
      unknown_urls.insert (url);
    else
      known_urls.insert (url);
  }

  mtx.lock();

  // Ampliar la tabla hash
  insert_url(unknown_urls);

  unsigned long index_base = url_to_index[element.base_url];

  for (const auto& url : element.linked_urls) {
    links_matrix[index_base].push_back(url_to_index[url]);
  }

  mtx.unlock();
}

std::vector<double> PageRank::get_rank(const std::vector<std::string>& urls) {
  std::vector<unsigned long> indices;
  for (const auto& url : urls) {
    indices.push_back(url_to_index[url]);
  }

  // Crear matriz de enlaces y normalizar sus valores
  unsigned long n = links_matrix.size();
  Matrix m_1 (n, n);

  std::cout << "0 " << links_matrix.size() << std::endl;

  mtx.lock();

  for (unsigned i = 0; i < links_matrix.size(); i++) {
    std::vector<unsigned> content;

    for (const auto& index : links_matrix[i]) {
      content.push_back(index);
    }

    unsigned n = content.size();

    for (const auto& index : content) {
      m_1.set(i, index, 1.0 / n);
    }
  }

  mtx.unlock();

  double u = 1.0 / n;


  // Ajuste de posibles ceros en las probabilidades
  for (unsigned i = 0; i < n; i++) {
    for (unsigned j = 0; j < n; j++) {
      double v = c * m_1.get(i, j) + (1 - c) * u;
      m_1.set(i, j, v);
    }
  }

  // 3. Resolver el sistema
  Matrix m_2;


  gauss (m_1, m_2);

  std::vector<long double> auto_vals(n);
  std::vector<long double> mod_auto_vals(n);
  double d = 0;

  // Calcular autovalores
  for (unsigned i = 0; i < n; i++) {
    double aux = m_2.get(i, i);
    auto_vals[i] = aux;
    if (aux < 0.00000001)
      std::cout << "PROBLEMAS" << std::endl;
    d += std::fabs(aux);
  }

  // Autovalores en módulo
  for (unsigned i = 0; i < n; i++) {
    mod_auto_vals[i] = auto_vals[i] / d;
  }

  // Mayor autovalor en módulo
  double max = -2183490234;
  unsigned index = 0;
  for (unsigned i = 0; i < n; i++) {
    double aux = mod_auto_vals[i];
    if (max < aux) {
      max = aux;
      index = i;
    }
  }

  // Mayor autovalor no negativo en módulo
  double val = auto_vals[index];
  std::vector<double> auto_vector(n);

  // Cálculo de los valores necesarios del autovector
  std::vector<double> result;
  for (const unsigned long i : indices) {
    double a = std::fabs(auto_vector[i] = m_2.get(i, i) - val);
    result.push_back(a);
  }

  return result;

  //return ac;
}

