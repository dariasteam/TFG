/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <queue>
#include <functional>

#include "websearch.h"

#include "lib_async/lib_asynchronous.hpp"

int main(int n_args, char** args) {

  std::string config_file;
  std::string seed_file;

  WebSearch web_search;

  bool b;

  if (n_args > 3)
    b = web_search.prepare(args[1], args[2]);
  else
    b = web_search.prepare();

  if (b) {
    web_search.init();
  }

  return 0;
}
