/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UI_H
#define UI_H

#include <iostream>
#include <string>
#include <curl/curl.h>
#include <queue>
#include <vector>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <sstream>
#include <iomanip>
#include <sstream>

#include "lib_async/lib_asynchronous.hpp"

#include "reverseindex.h"
#include "crawler.h"
#include "htmlparser.h"
#include "urlparser.h"
#include "searchengine.h"
#include "ngram.h"

/**
 * @todo write docs
 */
class UI : public asyncObject {
private:
  ReverseIndex* index;
  Crawler* crawler;
  HTMLParser* html_parser;
  URLParser* url_parser;
  SearchEngine* engine;
  NGram* ngram;

  bool master_handler (std::string input);
  bool menu_loop ();

  void show_header ();
  bool wait_for_user_input ();

  void search ();

  bool alive;

  bool can_print;
public:
  UI(ReverseIndex* ind, Crawler* craw, HTMLParser* html,
     URLParser* url, SearchEngine* eng, NGram* ngr);

  void start();
};

#endif // UI_H
