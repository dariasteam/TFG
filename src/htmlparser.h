/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HTMLPARSER_H
#define HTMLPARSER_H

#include <iostream>
#include <string>
#include <queue>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <sstream>
#include <memory>
#include <mutex>
#include <algorithm>

#include "xmlparser.h"
#include "reverseindex.h"
#include "ngram.h"
#include "defaultfilesgenerator.h"

#include "lib_async/lib_asynchronous.hpp"
#include "lib_jsoncpp/parser.hpp"
#include "lib_jsoncpp/serializable.h"

class XMLCondition : public json::Serializable {
public:
  std::string tag;
  std::string key;
  std::vector<std::string> values;

  SERIAL_START
    tag,
    key,
    values
  SERIAL_END
};

class XMLInfo : public json::Serializable {
public:
  std::vector<XMLCondition> inclusion_conditions;
  std::vector<XMLCondition> exclusion_conditions;
  std::vector<std::string> void_elements;

  SERIAL_START
    "inclusions",    inclusion_conditions,
    "exclusions",    exclusion_conditions,
    "void_elements", void_elements
  SERIAL_END
};

struct ContainerHTMLParser {
  std::shared_ptr<std::string> content;
  std::string url;
};

/**
 * @todo write docs
 */
class HTMLParser : public asyncDispatcher <ContainerHTMLParser> {
private:
  XMLParser xml_parser;

  NGram* ngram;
  ReverseIndex* reverse_index;

  void on_queue_empty();
  void on_queue_full();

  bool have_been_full;

  bool delivering_to_ngram;

  bool load_hmlparser_file (const std::string& htmlparser_file);

public:
  HTMLParser (ReverseIndex* ind, NGram* ng) :
    ngram (ng),
    reverse_index (ind),
    have_been_full (false),
    delivering_to_ngram (true)
  {
    load_hmlparser_file(DEFAULT_HTML_PARSER_PATH);

    ng->try_to_resume_delivering = [&] {
      on_resume_delivering_to_ngram ();
    };
  }

  void operation (ContainerHTMLParser element);

  void on_resume_delivering_to_ngram ();

  // SIGNAL
  std::function<void (void)> try_to_resume_crawler;
  std::function<void (void)> to_logger_full;
  std::function<void (void)> to_logger_empty;
};

#endif // HTMLPARSER_H
