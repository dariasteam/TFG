/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2018  Daniel Darias Sánchez <dariasteam94@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CRAWLER_H
#define CRAWLER_H

#include <iostream>
#include <string>
#include <curl/curl.h>
#include <queue>
#include <vector>
#include <regex>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <sstream>
#include <memory>
#include <mutex>
#include <atomic>
#include <list>

#include "htmlparser.h"
#include "urlparser.h"
#include "reverseindex.h"

#include "lib_async/lib_asynchronous.hpp"

/**
 * @todo write docs
 */

class Crawler : public asyncDispatcher<std::string> {
private:
  URLParser* url_parser;
  HTMLParser* html_parser;
  ReverseIndex* index;

  std::list <std::function <void (bool)>> wake_ups;

  std::unordered_set<std::string> visited_urls;     // Should be using the reverse index for this
  std::queue<std::string> urls_to_visit;
  bool active;

  int counter;

  // needed by libcurl
  static size_t WriteCallback(void *contents, size_t size,
                                              size_t nmemb,
                                              void *userp);
public:
  Crawler(URLParser* urlparser, HTMLParser* tokenparser, ReverseIndex* ind);

  void operation (std::string url);

  // slots
  void on_add_urls (std::unordered_set<std::string> urls);
  void on_try_to_resume ();
};

#endif // CRAWLER_H
