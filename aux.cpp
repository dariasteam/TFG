#include <dlfcn.h>
#include <iostream>

#include "src/websearch.h"

using namespace std;

int main(int argc, char **argv)
{
  void* handle = dlopen("./dyn_websearch.so", RTLD_LAZY);

  WebSearch* (*create)();
  void (*destroy)(WebSearch*);

  create = (WebSearch* (*)())dlsym(handle, "create_object");
  destroy = (void (*)(WebSearch*))dlsym(handle, "destroy_object");

  WebSearch* myClass = (WebSearch*)create();
	std::cout << myClass << std::endl;
  destroy( myClass );
}
