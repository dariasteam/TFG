CC=g++
CXXFLAGS= -std=c++11 -g -O2 -Wall -pedantic -lcurl -pthread -lleveldb

OBJS = 	src/ngram.o \
	src/searchengine.o \
	src/pagerank.o \
	src/logger.o \
	src/urlnormalizer.o \
	src/xmlparser.o \
	src/ui.o \
	src/reverseindex.o \
	src/urlparser.o \
	src/htmlparser.o \
	src/crawler.o \
	src/lib_jsoncpp/object.o \
	src/lib_jsoncpp/manager.o \
	src/lib_jsoncpp/parser.o \
	src/lib_jsoncpp/serializable.o \
	src/lib_jsoncpp/abstract_serializable.o \
	src/math/matrix.o \
	src/defaultfilesgenerator.o 	

all: ${OBJS}
	$(CC) $(CXXFLAGS) src/websearch.cpp src/main.cpp -o WebSearch ${OBJS}

dynamic: ${OBJS}
	$(CC) $(CXXFLAGS) -shared src/websearch.cpp src/dynamic_library/library.cpp -o dyn_websearch.so ${OBJS}

clean:
	rm -rf src/*.o WebSearch
